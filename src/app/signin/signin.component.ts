import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { AuthService } from "../auth.service";
import { auth } from 'firebase/app';
import { AngularFireAuth  } from "@angular/fire/auth";
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public obj ={user:'mmm' , bol:'true'} ;
  changeDetectorRef: ChangeDetectorRef
  constructor(public afAuth: AngularFireAuth,changeDetectorRef: ChangeDetectorRef) { 
    this.changeDetectorRef = changeDetectorRef;
  }

  ngOnInit() {
  }
  FacebookAuth() {
    console.log('hello');
   return this.AuthLogin(new auth.FacebookAuthProvider());
 }  
 GoogleAuth() {
   return this.AuthLogin(new auth.GoogleAuthProvider());
 }  

AuthLogin(provider) {
  this.obj.user = 'mirette';
  console.log('hello');
  return this.afAuth.auth.signInWithPopup(provider)
  .then(result => {
   this.obj.user = result['additionalUserInfo']['profile']['email'];
  
    this.obj.bol = 'false';
    this.changeDetectorRef.detectChanges();
    console.log(this.obj.user);
      // console.log(result['additionalUserInfo']['profile']);
      console.log('You have been successfully logged in!')
  }).catch((error) => {
      console.log(error)
  })
  
  
}


}
